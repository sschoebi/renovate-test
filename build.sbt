import Dependencies._

import scala.util.Properties.envOrElse

ThisBuild / scalaVersion := "2.13.8"
ThisBuild / version := "0.1.0-SNAPSHOT"
ThisBuild / organization := "com.example"
ThisBuild / organizationName := "example"
ThisBuild / useCoursier := false //due gitlab header issue switches back to ivy

//For gitlab private maven repo connection
enablePlugins(GitlabPlugin)
import com.gilcloud.sbt.gitlab.GitlabPlugin
GitlabPlugin.autoImport.gitlabDomain := "gitlab.com"
credentials += Credentials(
  "gitlab",
  "gitlab.com",
  "Private-Token",
  envOrElse("PACKAGE_REGISTRY_ACCESS_TOKEN", "")
)
resolvers += "gitlab-maven" at "https://gitlab.com/api/v4/projects/38102196/packages/maven/"


libraryDependencies += "com.example" %% "scala-base-lib" % "0.0.3"

enablePlugins(JibPlugin)

assembly / assemblyMergeStrategy := {
  case PathList("META-INF", xs @ _*) => MergeStrategy.discard
  case x                             => MergeStrategy.first
}
